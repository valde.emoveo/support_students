#ifndef NODE_H
#define NODE_H

#include "ellist.h"

typedef struct Node Node;
struct Node {
  char *key;
  ElList *top_value;
  Node *left;
  Node *right;

  int   height;
};

Node* allocate_node(char const *key, unsigned value);
void   dealloc_node(Node *node);
void clear_links_node(Node *node);

Node*     srch_node(Node *node, char const *key);
void    insert_node(Node **node, char const *key, unsigned value);
int     remove_node(Node **node, char const *key);
void   display_node(Node* node);

void  display_like_tree(Node *node, int level);
void       preoder_node(Node* node);
void preorder_less_node(Node* node, char const *key);


#endif /* NODE_H */
