#include "node.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>




Node* allocate_node(char const *key, unsigned value) {
  if (key == NULL) {
    return NULL;
  }
  Node *node   = (Node*) malloc(sizeof(Node));
  node->key    = (char*) malloc((strlen(key) + 1) * sizeof(char));
  strcpy(node->key, key);
  node->top_value = create_ellist(value);
  node->left   = NULL;
  node->right  = NULL;
  node->height = 0;

  return node;
}
void dealloc_node(Node *node) {
  if (node == NULL) {
    return;
  }

  erase_list(node->top_value);
  free(node->key);
  free(node);
}
void clear_links_node(Node *node) {
  if (node == NULL) {
    return;
  }

  clear_links_node(node->left);
  clear_links_node(node->right);
  dealloc_node(node);
}
Node* srch_node(Node *root, char const *key) {
  while (root != NULL) {
     int check = strcmp(root->key, key);
     if (check == 0) {
       break;
     }

     if (check > 0) {
       root = root->left;
     } else {
       root = root->right;
     }
  }
  return root;
}
/*
void insert_fixup(Node *node, Node *new_node) {
  if (strcmp(node->key, new_node->key) > 0) {
    if (node->left != NULL) {
      insert_fixup(node->left, new_node);
    } else {
      node->left = new_node;
      new_node->height = node->height + 1;
    }
  } else {
    if (node->right != NULL) {
      insert_fixup(node->right, new_node);
    } else {
      node->right = new_node;
      new_node->height = node->height + 1;
    }
  }
  // check_true;
  //insert_fixup(node->left, new_node);
}
*/
int get_height(Node *node) {
  if (node == NULL) {
    return -1;
  }
  return node->height;
}
void update_height(Node *node) {
  int left_h  = get_height(node->left);
  int right_h = get_height(node->right);
  if (left_h > right_h) {
    node->height = left_h + 1;
  } else {
    node->height = right_h + 1;
  }
}
Node* rotate_left(Node *node) {
  Node *center = node->right;
  node->right     = center->left;
  center->left = node;

  update_height(node);
  update_height(center);

  return center;
}
Node* rotate_right(Node *node) {
  Node *center = node->left;
  node->left = center->right;
  center->right = node;

  update_height(node);
  update_height(center);

  return center;
}
int factor_balance(Node *node) {
  return get_height(node->right) - get_height(node->left);
}
Node* balanced(Node *node) {
  int factor = factor_balance(node);

  if (factor < -1) {
    if (factor_balance(node->left) > 0) {
      node->left = rotate_left(node->left);
    }

    return rotate_right(node);
  } else if (factor > 1) {
    if (factor_balance(node->right) < 0) {
      node->right = rotate_right(node->right);
    }

    return rotate_left(node);
  }

  update_height(node);

  return node;
}
Node* insert_balanced(Node *node, Node *new_node) {
  if (node == NULL) {
    return new_node;
  } else if (strcmp(node->key, new_node->key) > 0) {
    node->left = insert_balanced(node->left, new_node);
  } else {
    node->right = insert_balanced(node->right, new_node);
  }

  return balanced(node);
}
void insert_node(Node **node, char const *key, unsigned value) {
  if (*node == NULL) {
    *node = allocate_node(key, value);
    return;
  }

  Node *new_node = srch_node(*node, key);
  if (new_node != NULL) {
    printf("Found duplicate key\n");
    insert_list(new_node->top_value, value);
    return;
  }
  new_node = allocate_node(key, value);
  *node = insert_balanced(*node, new_node);
//  insert_fixup(*node, new_node);
} 
Node* min_node(Node *node) {
  if (node == NULL) {
    return NULL;
  }
  while (node->left != NULL) {
    node = node->left;
  }
  return node;
}
void swap_node(Node *n1, Node *n2) {
  // swap content 
  //Node tmp      = *n1;
  Node tmp;
  tmp.key       = n1->key;
  tmp.top_value = n1->top_value;

  n1->key       = n2->key;
  n1->top_value = n2->top_value;

  n2->key       = tmp.key;
  n2->top_value = tmp.top_value;
}
Node* subremove_node(Node *node, char const *key) {
  if (node == NULL) {
    return NULL;
  }
  int check = strcmp(node->key, key);
  if (check != 0) {
    Node **direction = NULL;
    if (check > 0) {
      direction = &node->left;
    } else {
      direction = &node->right;
    }

    *direction = subremove_node(*direction, key);
    return balanced(node);
  }

  node->top_value = remove_old_list(node->top_value);
  if (node->top_value != NULL) { // if exist more info inside node
    return node;
  }
  
  if (node->left != NULL && node->right != NULL) {
    // swap slave
    Node *slave = min_node(node->right);
    swap_node(slave, node);
    node->right = subremove_node(node->right, key);
    return balanced(node);
  } 

  Node *child = node->right;
  if (node->left != NULL) {
    child = node->left;
  }

  dealloc_node(node);
  return child;
}
int remove_node(Node **node, char const *key) {
  *node = subremove_node(*node, key);
  return 0;
}
void display_like_tree(Node *node, int level) {
  if (node == NULL) {
    return;
  }
  display_like_tree(node->right, level + 1);
  for (int i = -5; i < level; ++i) {
    printf("==");
  }
  display_node(node); 
  display_like_tree(node->left, level + 1);
}
void display_node(Node* node) {
  printf("%s", node->key);
  printf(" Info: ");
  display_list(node->top_value);
}
void preoder_node(Node* node) {
  if (node == NULL) {
    return;
  }
  display_node(node);
  preoder_node(node->left);
  preoder_node(node->right);
}
void preorder_less_node(Node* node, char const *key) {
  if (node == NULL) {
    return;
  }

  int check = strcmp(node->key, key);
  if (check < 0) {
    display_node(node);
    preorder_less_node(node->left, key);
    preorder_less_node(node->right, key);
  } else {
    preorder_less_node(node->left, key);
  }
}
