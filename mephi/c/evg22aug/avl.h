#ifndef AVL_TREE
#define AVL_TREE

#include "node.h"

typedef struct AvlTree AvlTree;
struct AvlTree {
  Node *root; 
};

AvlTree create_tree();
void    delete_tree(AvlTree *tree);

int         srch_tree(AvlTree *tree);
int       insert_tree(AvlTree *tree);
int       remove_tree(AvlTree *tree);
int     preorder_tree(AvlTree *tree);
int special_srch_tree(AvlTree *tree);
int         like_tree(AvlTree *tree);

#endif /* AVL_TREE */
