#include "avl.h"
#include <stdlib.h>
#include <stdio.h>



AvlTree create_tree() {
  return (AvlTree) {.root = NULL};
}
void delete_tree(AvlTree *tree) { 
  clear_links_node(tree->root);
}
int srch_tree(AvlTree *tree) {
  // позже взглянуть на формулировку задания
  char key[65];
  printf("Enter key: ");
  scanf("%s", key); // заменить на свой код
  Node *node = srch_node(tree->root, key);
  if (node == NULL) {
    return 2;
  }
  int release;
  printf("Enter release: ");
  scanf("%d", &release);
  
  display_list_release(node->top_value, release);
  return 0;
}
int insert_tree(AvlTree *tree) {
  char key[65];
  printf("Enter key: ");
  scanf("%s", key); // заменить на свой код
                    
  unsigned info;
  printf("Enter info: ");
  scanf("%u", &info); // заменить на свой код
                                     
  insert_node(&tree->root, key, info);
  return 0;
}
int remove_tree(AvlTree *tree) {
  char key[65];
  printf("Enter key: ");
  scanf("%s", key); // заменить на свой код
  
  printf("=================\n");
  display_list(tree->root->top_value);
  printf("=================\n");

  return remove_node(&tree->root, key);                
}
int preorder_tree(AvlTree *tree) {
  char key[65]; // строчка произвольного размера
  printf("Enter key: ");
  char tmp;
  scanf("%*c%c", &tmp);
  if (tmp != '\n') {
    key[0] = tmp; 
    int i = 1;
    for (; i < 64; ++i) {
      scanf("%c", &tmp);
      if (tmp == '\n') {
        break;
      }
      key[i] = tmp;
    }
    key[i] = '\0';
    preorder_less_node(tree->root, key);
  } else {
    preoder_node(tree->root);
  }

  return 0;
}
int special_srch_tree(AvlTree *tree) {
  if (tree == NULL) {
    return 0;
  }
//  special_srch_node(&tree->root);
  return 0;
}
int like_tree(AvlTree *tree) {
  display_like_tree(tree->root, 0);
  return 0;
}
