#ifndef LIST_H
#define LIST_H

typedef struct ElList ElList;
struct ElList {
  unsigned  value;
  ElList   *next;
};


ElList* create_ellist(unsigned value);
void    delete_ellist(ElList *ellist);

void      insert_list(ElList *list, unsigned value);
ElList* remove_old_list(ElList *list);
void       erase_list(ElList *list);
void     display_list(ElList *list);
void display_list_release(ElList *list, int release);

#endif // LIST_H
