#include "avl.h"
#include <stdio.h>
#include <stdlib.h>



char const* conv_stts(int stts) {
  switch (stts) {
    case 0: return "All right";
    case 1: return "System error";
    case 2: return "Not found";
    case 3: return "Emptry";
    default: return "None";
  }
}
void stts_ext(int stts) {
  printf("%s - \n", conv_stts(stts)); 
}


int main() {
  AvlTree tree = create_tree();
  
  int (*funcs[])(AvlTree*) = {NULL, srch_tree, insert_tree, 
                              remove_tree, preorder_tree, 
                              special_srch_tree, like_tree};
  
  int choice;
  while (1) {
    printf("\n\n1. Search\n2. Insert\n3. Remove\n4. Preorder\n"
           "5. Special srch\n6. Like tree\n0.Exit\n>");

    scanf("%d", &choice);
    // проверить не заходит ли за границы
    //    не моя функция
    if (choice == 0) {
      break;
    }
    int res = funcs[choice](&tree);
    stts_ext(res);
  }

  delete_tree(&tree);
  return 0;
}
