#include "ellist.h"
#include <stdio.h>
#include <stdlib.h>




ElList* create_ellist(unsigned value) {
  ElList *ellist = (ElList*) malloc(sizeof(ElList));
  ellist->value  = value;
  ellist->next   = NULL;
  return ellist;
}
void delete_ellist(ElList *ellist) {
  if (ellist == NULL) {
    return;
  }
  free(ellist);
}
void insert_list(ElList *list, unsigned value) {
  if (list == NULL) {
    return;
  }

  while (list->next != NULL) {
    list = list->next;
  }
  list->next = create_ellist(value);
}
ElList* remove_old_list(ElList *list) {
  if (list == NULL) {
    return NULL;
  }
  ElList *next = list->next;
  delete_ellist(list);
  return next;
}
void erase_list(ElList *list) {
  while (list != NULL) {
    list = remove_old_list(list);
  }
}
void display_list(ElList *list) {
//  int count = 0;
  while (list != NULL) {
 //   ++count;
    printf("[%u]\t", list->value);
    list = list->next;
  }
  printf("\n");
  //printf("ALL ELEMENT'S LIST - %d\n", count);
}
void display_list_release(ElList *list, int release) {
  while (list != NULL && release > 0) {
    list = list->next;
    --release;
  }
  if (list == NULL || release != 1) {
    printf("Not found by release\n");
  }
  printf("Result: %u\n", list->value);
}
