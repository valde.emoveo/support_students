#include <stdio.h>
#include <stdlib.h>


char* read_str() {
  size_t size = 16;
  char *str = malloc(sizeof(char) * 16);
  for (size_t i = 0; i < size; ++i) {
    scanf("%c", str + i);
    if (str[i] == '\n') {
      str[i] = '\0';
      break;     
    }
    if (i + 1 == size) { // увеличить размер строки, для большего кол-ва букв
      size *= 2;
      str = realloc(str, sizeof(char) * size);
    }
  }
  return str;
}

int main() {
  printf("Enter smth: ");
  char* result = read_str();
  printf("Result: %s\n", result);
  free(result);
  return 0;
}
