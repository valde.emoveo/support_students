#include "tree.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>



struct Tree* create_tree()
{
  return calloc(1, sizeof(struct Tree));
}
void delete_tree(struct Tree *tree)
{
  clear_all_nodes(tree->root);
  free(tree);
}
int deep_tree(const struct Tree *tree) {
  int deep = 0;
  if (tree->root != NULL)
  { deep = deep_node(tree->root); }

  return deep;
}
void treelike_display(const struct Tree *tree) {
  int deep = deep_tree(tree);

#define VS_PLATE        1
#define LINK_NOT_PARENT 2
  //       8    
  //    7     9
  //
  
  /*
  int count_plate = pow(2, deep); // Количество ГЛУБОЧАЙШИХ листьев

  int count_space = (count_plate - 2) * LINK_NOT_PARENT + VS_PLATE;
  
  */
  /*
  // Заняться центрированием
  for (int i = 0; i < deep; ++i) {
    // print_left, print_right
    
    printf("\n");
  }
  */
}
void add_in_tree(struct Tree *tree, unsigned key, unsigned value)
{
  // Дублирование разрешено
  //    на нужное место key, должны вставлять info образуя список
  //
  // 1. Найти ключ
  // Если нету -> new Node
  //      есть -> new Info in Node
  if (tree->root == NULL)
    tree->root = create_node(key, value);
  else
    insert_node(tree->root, create_node(key, value));
}
int del_in_tree(struct Tree *tree, unsigned key, unsigned release)
{
  if (tree->root == NULL)
  { return EXIT_FAILURE; } // 1 - ???
/*
  if (tree->root->key == key) { // change root
    if (delete_info_by_release(&tree->root->info, release) == 0 && 
        tree->root->info == NULL) { // Если после удаления элемента по номеру версии список (INFO) пуст, то необходимо сменить узел
      remove_node(&tree->root);
      //free(tree->root);
       // tree->root = NULL;
    }
  }
  */
  int status_remove = remove_node(tree->root, key, release);

  return status_remove;
}
void display_tree(const struct Tree *tree)
{
  if (tree->root == NULL)
  { return; }

  display_node(tree->root, 0);
}
struct Info** find_key_in_tree(const struct Tree *tree, unsigned key)
{
  return find_key(tree->root, key); 
}
// special find
const struct Node* find_near_in_tree(const struct Tree *tree, unsigned value)
{
  return find_near(tree->root, value);
}

