#ifndef HANDLE_H 
#define HANDLE_H 

#include <stdio.h>

enum ProgramProccess {
  SYSTEM_ERROR = 2,
  INVALID_INPUT
};

int      str_to_ll(char const *str, long long *dest);
int   scanf_string(FILE *stream, char **string);
int scanf_unsigned(FILE *stream, unsigned *dest);

#endif // HANDLE_H 
