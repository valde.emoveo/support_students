#ifndef NODE_H 
#define NODE_H 

#include <stddef.h>
#include "info.h"

struct Node {
  unsigned     key;
  struct Info *info;

  struct Node *par;
  struct Node *left;
  struct Node *right;
};

struct Node*      create_node(unsigned key, unsigned value);
void          clear_all_nodes(struct Node *node);

void         insert_node(struct Node *root, struct Node *node);
int          remove_node(struct Node *root, unsigned key, unsigned release);
void        display_node(const struct Node *node, size_t deep);

int deep_node(const struct Node *node);

struct Node* search_node(struct Node *node, unsigned key);
struct Info**   find_key(struct Node *node, unsigned key);
struct Node*    find_near(struct Node *node, unsigned key);


#endif // NODE_H 

