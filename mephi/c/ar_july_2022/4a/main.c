#include <stdio.h>
#include <stdlib.h>
#include "tree.h"




int main() {
  struct Tree *tree = create_tree();
  int choice;
  unsigned key, info, release;


  //       8    
  //    7     9
//  add_in_tree(tree, 10, 1);
  add_in_tree(tree, 8, 1);
  add_in_tree(tree, 7, 1);
  add_in_tree(tree, 9, 1);

  treelike_display(tree);
  /*
  for (int i = 0; i < 7; ++i) {
    add_in_tree(tree, 11 + i, 1);
  }
  */


  printf("Deep: %d\n", deep_tree(tree));

  /*
  while (1) {
    printf("1. Add\n2. Delete\n3. Display\n4. Search by key\n5. Search near\n0. Exit\n");
    scanf("%d", &choice);
    if (choice == 0) 
    { break; }

    if (choice == 1) {
      printf("ENTER KEY: ");
      scanf("%u", &key);
      printf("ENTER INFO: ");
      scanf("%u", &info);
      add_in_tree(tree, key, info);
    }
    else if (choice == 2) {
      printf("ENTER KEY: ");
      scanf("%u", &key);
      printf("ENTER RELEASE: ");
      scanf("%u", &release);
      del_in_tree(tree, key, release);
    }
    else if (choice == 3) {
      display_tree(tree);
    }
    else if (choice == 4) {
      printf("ENTER KEY: ");
      scanf("%u", &key);
      struct Info** list = find_key_in_tree(tree, key);
      printf("RESULT:\n");
      for (int i = 0; list[i] != NULL; ++i) {
        print_one_info(list[i]);
      }
      free(list);
    }
    else if (choice == 5) {
      printf("ENTER KEY: ");
      scanf("%u", &key);
      const struct Node *res_node = find_near_in_tree(tree, key);
      if (res_node != NULL) {
        print_info(res_node->info);
      }
    }
  }
  */

  delete_tree(tree);
  return 0;
}

