#ifndef TREE_H
#define TREE_H

#include "node.h"

struct Tree
{
  struct Node *root;
};

struct Tree* create_tree();
void         delete_tree(struct Tree *tree);

int deep_tree(const struct Tree *tree);

void     add_in_tree(struct Tree *tree, unsigned key, unsigned value);
int      del_in_tree(struct Tree *tree, unsigned key, unsigned release);
void    display_tree(const struct Tree *tree);

void treelike_display(const struct Tree *tree);

struct Info**         find_key_in_tree(const struct Tree *tree, unsigned key);
// special find
const struct Node*   find_near_in_tree(const struct Tree *tree, unsigned value);

#endif // TREE_H

