#include "node.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>




struct Node* create_node(unsigned key, unsigned value) {
  struct Node *new_node = calloc(1, sizeof(struct Node));
  new_node->key = key;

  new_node->info = calloc(1, sizeof(struct Info));
  new_node->info->value = value;
  
  return new_node;
}
void clear_all_nodes(struct Node *node)
{
  if (node == NULL)
  { return; }

  clear_all_nodes(node->right);
  clear_all_nodes(node->left);

  delete_info(node->info);
  free(node);
}
void insert_node(struct Node *root, struct Node *node)
{
  // Дублирование разрешено
  //    на нужное место key, должны вставлять info образуя список
  //
  // 1. Найти ключ
  // Если нету -> new Node
  //      есть -> new Info in Node
  if (root == NULL)
  { return; }

  if (root->key == node->key) { // Добавление в список
    push_info(&root->info, node->info);
    // Удаление node кроме info
    free(node);
    return;
  }

  struct Node **direction = NULL;
  if (root->key < node->key) {
    direction = &root->right;
  }
  else if (root->key > node->key) {
    direction = &root->left;
  }

  if (*direction == NULL) {
    node->par = root;
    *direction = node;
  }
  else {
    insert_node(*direction, node);
  }
}
struct Node* get_min_node(struct Node *node)
{
  if (node == NULL)
  { return NULL; }

  while (node->left != NULL)
  { node = node->left; }

  return node;
}
struct Node* get_max_node(struct Node *node)
{
  if (node == NULL)
  { return NULL; }

  while (node->right != NULL)
  { node = node->right; }

  return node;
}
struct Node* who_near(struct Node *node1, struct Node *node2, unsigned key) {
  // Кто ближе, первый или второй
  if (node1 == NULL)
  { return node2; }

  if (node2 == NULL)
  { return node1; }

  
  if (llabs((long long)node1->key - key) > llabs((long long)node2->key - key))  // fabs - взятие числа по модулю
  { return node1; }

  return node2;
}
struct Node* find_near(struct Node *node, unsigned key)
{
  if (node == NULL)
  { return NULL; }
  struct Node *min_node = get_min_node(node);
  struct Node *max_node = get_max_node(node);

  return who_near(min_node, max_node, key);
}
void linking_par(struct Node *node, struct Node *jertva) {
  // Определяем где у родителя стоит удялемый элемент
  if (node->par->left != NULL && node->par->left->key == node->key) {
    node->par->left  = jertva;
  }
  else {
    node->par->right = jertva;
  }
}
void double_link(struct Node *node) {
  struct Node *near = find_near(node, node->key);
  // node = near;
  node->key  = near->key;
  node->info = near->info;

  // разобраться со связями near
  if (near->left == NULL && near->right == NULL) {
    linking_par(near, NULL);
  }
  else if (near->left  != NULL) {
    near->left->par = near->par;
    linking_par(near, near->left);
  }
  else if (near->right != NULL) {
    near->right->par = near->par;
    linking_par(near, near->right);
  }
  free(near);
}
int remove_node(struct Node *root, unsigned key, unsigned release) {
  struct Node *node = search_node(root, key);
  if (node == NULL) 
  { return EXIT_FAILURE; }

  int status_delete = delete_info_by_release(&node->info, release);
  if (status_delete == EXIT_FAILURE) 
  { return EXIT_FAILURE; }

  if (node->info != NULL)
  { return EXIT_SUCCESS; }

  // node == NULL
  if (node->left == NULL && node->right == NULL) {
    linking_par(node, NULL);
    free(node);
  }
  else if (node->left != NULL && node->right == NULL) {
    node->left->par = node->par;
    linking_par(node, node->left);
    free(node);
  }
  else if (node->left == NULL && node->right != NULL) {
    node->right->par = node->par;
    linking_par(node, node->right);
    free(node);
  }
  else { // left != NULL && right != NULL
    double_link(node);
  }
  return EXIT_SUCCESS;
}
void display_node(const struct Node *node, size_t deep)
{
  if (node == NULL)
  { return; }

  display_node(node->left, deep + 1);
  display_node(node->right, deep + 1);
  
  for (size_t i = 0; i < deep; ++i)
  {
    printf("\t");
  }
 // printf("%5d", node->key);
  printf("KEY: %d", node->key);
  print_info(node->info);
  printf("\n");
}
int deep_node(const struct Node *node) {
  if (node == NULL)
  { return 0; }

  // find max deep
  int left_deep  = deep_node(node->left);
  int right_deep = deep_node(node->right);

  int max_deep = left_deep >= right_deep ? left_deep : right_deep; 
  return max_deep + 1; // 1 - текущий уровень глубины
}
struct Node* search_node(struct Node *node, unsigned key)
{ 
  if (node == NULL || node->key == key)
  { return node; }

  if (node->key < key) {
    return search_node(node->right, key);
  }
  else {
    return search_node(node->left, key);
  }
} 
struct Info** find_key(struct Node *node, unsigned key)
{
  struct Node *res_find = search_node(node, key);

  struct Info **array = NULL;
  if (res_find != NULL)
  {
    array = list_to_array(res_find->info);   
  }
  return array;
}

