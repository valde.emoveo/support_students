#include "handle.h"
#include <stdlib.h>
#include <limits.h>



int scanf_string(FILE *stream, char **string) {
  if (*string != NULL || stream == NULL) { 
    // pointer of string should be null.
    return SYSTEM_ERROR;
  }

  *string = malloc(16 * sizeof(char));
  if (*string == NULL)
  {
    return SYSTEM_ERROR;
  }

  size_t len_str = 0;
  char sym;
  while (1) {
    sym = getc(stream);
    if (sym == '\0' || sym == '\n' || sym == EOF) { 
      break;
    }

    (*string)[len_str++] = sym; 

    if (len_str % 16 == 0) {
      *string = realloc(*string, (len_str * 2) * sizeof(char));
    }
  }
  (*string)[len_str++] = '\0'; 

  *string = realloc(*string, len_str * sizeof(char));
  return EXIT_SUCCESS;
}
int str_to_ll(char const *str, long long *dest) {
  if (str == NULL || dest == NULL) {
    return SYSTEM_ERROR;
  }

  int negative_num = 0;
  size_t i = 0;
  for (; str[i] != '\0'; ++i) {
    if (str[i] == '-') {
      negative_num += 1;  
    }
    else if (str[i] != '+') {
      break;
    }
  }

  if (str[i] == '\0') {
    return INVALID_INPUT;
  }

  *dest = 0;
  for (; str[i] != '\0'; ++i) {
    if (str[i] < '0' || '9' < str[i]) {
      return INVALID_INPUT;
    } 

    *dest = (*dest * 10) + (str[i] - '0');
  }

  if (negative_num % 2 == 1) {
    *dest *= (-1);
  }
  return EXIT_SUCCESS;
}
int scanf_unsigned(FILE *stream, unsigned *dest) {
  if (dest == NULL || stream == NULL) { 
    // pointer of string should be null.
    return SYSTEM_ERROR;
  }

  char *str_version = NULL;
  if (scanf_string(stream, &str_version)) {
    return SYSTEM_ERROR;
  }

  long long temp_number;
  if (str_to_ll(str_version, &temp_number) == INVALID_INPUT) {
    free(str_version);
    return EXIT_FAILURE;
  }

  free(str_version);
  if (0 < temp_number || temp_number > UINT_MAX) {
    return INVALID_INPUT;
  }
  
  *dest = temp_number;
  return EXIT_SUCCESS;
}
