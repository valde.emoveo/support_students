#include "info.h"
#include <stdio.h>
#include <stdlib.h>



void erase_info(struct Info **ptr)
{
  struct Info *trash = *ptr;
  *ptr = (*ptr)->next;
  free(trash);
}
void delete_info(struct Info *head)
{
  while (head != NULL)
  {
    erase_info(&head);
  }
}

int delete_info_by_release(struct Info **head, unsigned release)
{
  if (release == 1) {
    erase_info(head);
    return EXIT_SUCCESS;
  }

  struct Info *curr = *head;
  while (curr->next != NULL && release > 2) // Поиск эл-та с нужным номером
  {
    --release;
    curr = curr->next;
  }
  if (release == 2 && curr->next != NULL) // Если нашли элемент с нужным номером
  {
    erase_info(&(curr->next));
    return EXIT_SUCCESS;
  }

  return EXIT_FAILURE;
}
void push_info(struct Info **head, struct Info *elem)
{
  elem->next = *head;
  *head = elem;
}
void print_one_info(const struct Info *info)
{
  printf("|%5u|", info->value);
}
void print_info(const struct Info *head)
{
  while (head != NULL)
  {
    print_one_info(head);
    head = head->next;
  }
}
struct Info**  list_to_array(struct Info *head)
{
  if (head == NULL)
  { return NULL; }

  size_t size = 0;
  struct Info **array = malloc(sizeof(struct Info*) * 10);

  array[size++] = head;
  head = head->next;

  while (head != NULL)
  {
    if (size % 10 == 0) // Не хватает места
    {
      array = realloc(array, sizeof(struct Info*) * (size + 10));
    }
    array[size++] = head;
    head = head->next;
  }
  array[size++] = NULL;
  array = realloc(array, sizeof(struct Info*) * size);
  
  return array;
}

