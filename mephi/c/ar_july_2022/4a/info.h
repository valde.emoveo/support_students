#ifndef INFO_H
#define INFO_H

struct Info {
  unsigned     value;
  struct Info *next;
};

void delete_info(struct Info *head);
int  delete_info_by_release(struct Info **head, unsigned release);

void               push_info(struct Info **head, struct Info *elem); 
void          print_one_info(struct Info const *info);
void              print_info(struct Info const *head);
struct Info**  list_to_array(struct Info const *head);

#endif // INFO_H

