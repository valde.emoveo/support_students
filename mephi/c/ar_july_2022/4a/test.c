#include <stdio.h>
#include "handle.h"


int main() {
  unsigned number;
  printf("Enter number: ");
  while (scanf_unsigned(stdin, &number) == INVALID_INPUT) {
    printf("Enter again: ");
  }
  printf("Your number: %u\n", number);

  return 0;
}
