#include <stdio.h>
#include <stdlib.h>
#include "../common/input_func.h"
#include "stack.h"


void display_instruction() {
  printf("************************************************\n");
  printf("\tINFIX\t\t\tPOSTFIX\n");
  printf("Ex:\n");
  printf("1) A + B * C + D\t\tA B C * + D +\n");
  printf("2) (A + B) * (C + D)\t\tA B + C D + *\n");
  printf("3) A * B + C * D\t\tA B * C D * +\n");
  printf("4) A + B + C + D\t\tA B + C + D +\n");
  printf("\n\n\tVALID SYMBOL:\n");
  printf("0 - false\t1 - true\n");
  printf("! - not\t&& - and\t|| - or\n");
  printf("************************************************\n");
}

#ifdef STACK_ARRAY_H

Stack init() {
  return create_stack(sizeof(char), 16);
}

#elif STACK_LIST_H

Stack init() {
  return create_stack(sizeof(char));
}

#endif // STACK_LIST_H
       
bool is_logic_var(char sym) {
  return sym == '0' || sym == '1';
}
bool is_binary_operation(char sym) {
  return sym == '&' || sym == '|';
}
bool is_not(char sym) {
  return sym == '!';
}
char calc_and(char left, char right) {
  return left == '1' && right == '1' ? 1 : 0;
}
char calc_or(char left, char right) {
  return left == '1' || right == '1' ? 1 : 0;
}
bool calculation(Stack *expn) {
  // operand, operators
  char first;
  if (pop(expn, &first)) {
    return false;
  }

  if (is_not(first) == false && is_binary_operation(first) == false) {
    push(expn, &first);
    return false;
  }
  
  char second;
  if (pop(expn, &second)) { // need return if oprt - incorrect
    push(expn, &first);
    return false;
  }
  if (is_logic_var(second) == false) {
    push(expn, &second);
    push(expn, &first);
    return false;
  }

  char res;
  if (is_not(first)) {
    res = second == '0' ? '1' : '0';
  } else {
    char third;
    if (pop(expn, &third)) { // need return if oprt - incorrect
      push(expn, &second);
      push(expn, &first);
      return false;
    }
    if (is_logic_var(third) == false) {
      push(expn, &third);
      push(expn, &second);
      push(expn, &first);
      return false;
    }
    if (first == '&') {
      res = second == '1' && third == '1' ? '1' : '0';
    } else if (first == '|') { // == '|'
      res = second == '1' || third == '1' ? '1' : '0';
    }
  }

  push(expn, &res);
  return true;
}
bool valid_expression(char const *expn) {
  bool valid = true;
  for (size_t i = 0; expn[i] != '\0'; ++i) {
    if (expn[i] == ' ') {
      continue;
    } else if (is_binary_operation(expn[i])) {
      if (expn[i] != expn[i + 1]) {
        valid = false;
      }
      ++i; // skip second binary operation
    } else if (is_logic_var(expn[i]) == false && is_not(expn[i]) == false) {
      valid = false; 
    }
    
    if (valid == false) {
      printf("[%c = #%zu] = Invalid symbol\n", expn[i], i + 1);
      break;
    }
  }
  return valid;
}
void handle_expression(char const *expn) {
  if (valid_expression(expn) == false) {
    return;
  }

  Stack stack = init();
  for (size_t i = 0; expn[i] != '\0'; ++i) {
    if (expn[i] == ' ') { // skip (why not?)
      continue;
    }

    push(&stack, expn + i);
    if (is_binary_operation(expn[i])) {
      ++i; // skip second operator
           
      while (calculation(&stack))
      { continue; }
    } else if (is_not(expn[i])) {
      calculation(&stack);
    }
  }

  char res = 'n';
  if (pop(&stack, &res) == 0 && empty(&stack) && is_logic_var(res)) {
    printf("Result: %c\n", res);
  } else {
    printf("ERROR: Your expression can't calculate\n");
  }
  delete_stack(&stack);
}


int main() {
  display_instruction();
  
  char *expn = NULL;
  printf("Your expression: ");
  if (input_str(&expn) == NULL) {
    return 1;
  }
  
  handle_expression(expn);
  
  free(expn);
  return 0;
}
