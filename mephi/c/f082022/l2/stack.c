#include "stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


static char const* status_str(int status) {
  switch (status) {
  case EXIT_SUCCESS:   return "Nothing to broken";
  case NULL_STACK:     return "Pointer of stack is null";
  case STACK_EMPTY:    return "Stack contents are empty";
  case STACK_OVERFLOW: return "Stack overflow";
  case NOT_SET_DATA:   return "In cell of stack can't set data";
  case INCORRECT_DATA: return "Pointer of data can't be handle";
  default:             return "Unknown Status";
  }
}
void err_stack(int err) {
  printf("%s\n", status_str(err));
}

#ifdef STACK_ARRAY_H

bool empty(Stack const *stack) {
  if (stack == NULL) {
    return false;
  }
  return empty_array(&stack->array);
}
Stack create_stack(size_t sz_data, size_t length_array) {
  return (Stack) {
                  .array = create_array(sz_data, length_array)
                  };
}
void delete_stack(Stack *stack) {
  if (stack == NULL) {
    return;
  }

  delete_array(&stack->array);
}
int push(Stack *stack, void const *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  return push_back(&stack->array, data);
}
int pop(Stack *stack, void *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  return pop_back(&stack->array, data);
}

#elif STACK_LIST_H
       
bool empty(Stack const *stack) {
  if (stack == NULL)
    return false;

  return stack->top == NULL;
}
static Node* pop_back(Stack *stack) {
  if (empty(stack)) {
    return NULL;
  }
  Node *last_node = stack->top;
  stack->top = stack->top->next;

  last_node->next = NULL; // чтобы не было связи с другим
  return last_node;
}
Stack create_stack(size_t sz_data) {
  return (Stack) {
                  .sz_data = sz_data,
                  .top     = NULL
                 };
}
void delete_stack(Stack *stack) {
  if (stack == NULL) {
    err_stack(NULL_STACK);
    return;
  }

  Node *top_node = pop_back(stack);
  while (top_node != NULL) { // пока не закончиться инфа
    free(top_node->data);
    //unset_node(top_node);
    free(top_node);
    top_node = pop_back(stack);
  }
}
int push(Stack *stack, void const *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  if (data == NULL) {
    return INCORRECT_DATA;
  }
  
  // new_node
  Node *new_node = (Node*) malloc(sizeof(Node));
  if (new_node == NULL) {
    return NOT_SET_DATA;
  }
  new_node->data = (void*) malloc(stack->sz_data);
  if (new_node->data == NULL) {
    free(new_node);
    return NOT_SET_DATA;
  }
  memcpy(new_node->data, data, stack->sz_data);

  new_node->next = stack->top;
  stack->top = new_node;

  return EXIT_SUCCESS;
}
int pop(Stack *stack, void *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  if (data == NULL) {
    return INCORRECT_DATA;
  }

  Node *top_node = pop_back(stack);
  if (top_node == NULL) {
    return STACK_EMPTY;
  }
  memcpy(data, top_node->data, stack->sz_data);
  free(top_node->data);
  free(top_node);

  return EXIT_SUCCESS;
}

#endif // STACK_LIST_H
