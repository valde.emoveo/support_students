#ifndef ENTRY_H 
#define ENTRY_H 

#include <stdbool.h>

typedef struct Entry Entry;
struct Entry {

  union Parent {
    Entry *ptr;
    char  *str;
  } parent;
  
  char *key;
  char *info;
};


void      input_entry(Entry *entry);
void input_srch_entry(Entry *entry);

void  delete_entry(void *entry); // clear content
void display_entry(void *entry);
bool      is_entry(void *entry, void *cmp_data);
bool    this_child(void *entry, void *cmp_data);
bool   link_parent(void *entry, void *data);
void unlink_parent(void *entry);

#endif /*ENTRY_H */
