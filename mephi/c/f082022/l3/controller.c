#include "controller.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../common/input_func.h"


struct Controller {
  Table table;
};

Controller* init_controller(void) {
  size_t msize = 0;
  printf("Size of table: ");
  input_correct_size(&msize);
  
  Controller *ctrl = (Controller*) malloc(sizeof(Controller));
  if (ctrl == NULL) {
    exit(EXIT_FAILURE);
  }
  Table table = create_table(msize);
  memcpy(&ctrl->table, &table, sizeof(Table));
  return ctrl;
}
void close_controller(Controller *ctrl) {
  if (ctrl == NULL) {
    return;
  }

  delete_table(&ctrl->table);
  free(ctrl);
}
static int handle(Controller *ctrl, predicate condt, 
                    action_table foo)
{
  if (empty_table(&ctrl->table)) {
    return EMPTY;
  }
  Entry entry = {0};
  input_srch_entry(&entry);
  int status = foo(&ctrl->table, &entry, condt);
  free(entry.key);

  return status;
}
int search_data(Controller *ctrl) {
  if (ctrl == NULL) {
    return EXIT_FAILURE;
  }
  
  return handle(ctrl, is_entry, search_table);
}
int add_data(Controller *ctrl) {
  if (ctrl == NULL) {
    return EXIT_FAILURE;
  }
  if (full_table(&ctrl->table)) {
    return OVERFLOW;
  }

  Entry entry = {0};
  input_entry(&entry);
  
  int status = add_table(&ctrl->table, &entry);
  if (status != EXIT_SUCCESS) {
    free(entry.key);
    free(entry.info);
    if (entry.parent.str != NULL) {
      free(entry.parent.str);
    }
  }
  return status;
}
int del_data(Controller *ctrl) {
  if (ctrl == NULL) {
    return EXIT_FAILURE;
  } 
  return handle(ctrl, is_entry, erase_table);
}
int display_data(Controller *ctrl) {
  if (ctrl == NULL) {
    return EXIT_FAILURE;
  }
  if (empty_table(&ctrl->table)) {
    return EMPTY;
  }

  display_table(&ctrl->table);
  return EXIT_SUCCESS;
}
int del_w8_child(Controller *ctrl) {
  if (ctrl == NULL) {
    return EXIT_FAILURE;
  }
  if (empty_table(&ctrl->table)) {
    return EMPTY;
  }

  Entry entry = {0};
  input_srch_entry(&entry);

  int status = INVALID_PARENT;
  if (exist_table(&ctrl->table, &entry, this_child) == KEY_NOT_FOUND) {
    status = erase_table(&ctrl->table, &entry, is_entry);
  }

  free(entry.key);
  return status;
}
int search_child(Controller *ctrl) {
  if (ctrl == NULL) {
    return EXIT_FAILURE;
  }
  return handle(ctrl, this_child, search_table);
}
