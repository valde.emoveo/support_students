#ifndef LAB3_CONTROLLER_H
#define LAB3_CONTROLLER_H

#include "table.h"

typedef struct Controller Controller;
struct Controller;

Controller*  init_controller(void);
void        close_controller(Controller *ctrl);

int   search_data(Controller *ctrl);
int      add_data(Controller *ctrl);
int      del_data(Controller *ctrl);
int  display_data(Controller *ctrl);
int  del_w8_child(Controller *ctrl);
int  search_child(Controller *ctrl);

#endif // LAB3_CONTROLLER_H
