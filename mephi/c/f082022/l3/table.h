#ifndef TABLE_H
#define TABLE_H

#include "../common/array.h"
#include "entry.h"

enum StatusProcess {
  EMPTY    = 3,
  OVERFLOW,
  KEY_DUPLICATE,
  KEY_NOT_FOUND,
  INVALID_PARENT
};

typedef struct Table Table;
struct Table {
  Array array;
};

Table create_table(size_t msize);
void  delete_table(Table *table);

bool empty_table(Table const *table);
bool  full_table(Table const *table);

typedef int (*action_table)(Table*, Entry*, predicate);

int  search_table(Table *table, Entry *entry, predicate condt);
int     add_table(Table *table, Entry *entry);
int   erase_table(Table *table, Entry *entry, predicate condt);
int   exist_table(Table *table, Entry *entry, predicate condt);

int display_table(Table *table);

#endif /*TABLE_H*/
