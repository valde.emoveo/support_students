#include <stdio.h>
#include <stdlib.h>
#include "controller.h"
#include "../common/input_func.h"


void display_menu() {
  printf("1. SEARCH\n");
  printf("2. ADD\n");
  printf("3. DELETE\n");
  printf("4. CONTENT\n");
  printf("5. DELETE (if w8 children)\n");
  printf("6. SEARCH BY PARENT\n");
  printf("0. EXIT\n");
  printf("> ");
}
char const* status_to_str(int status) {
  switch (status) {
    case EXIT_SUCCESS:   return "All right!";
    case EXIT_FAILURE:   return "System failure";
    case EMPTY:          return "Table is empty";
    case OVERFLOW:       return "Table is full";
    case KEY_DUPLICATE:  return "Duplicate key";
    case KEY_NOT_FOUND:  return "Key not found";
    case INVALID_PARENT: return "Incorrect parent key";
    default:             return "Unknown status exit";
  }
}


int main() {
  Controller *controller = init_controller();
  
  int (*menu[])(Controller*) = {NULL, search_data, add_data, 
                                 del_data, display_data,
                                 del_w8_child, search_child}; 
  
  int choice = 0;
  while (1) {
    display_menu();
    while (input_int(&choice) || choice < 0 ||
            choice >= (int) (sizeof(menu) / sizeof(menu[0]))) {
      printf("Incorrect menu item\nEnter again: ");
    }

    if (choice == 0) {
      break;
    }

    int status = menu[choice](controller);
    printf("*******************************************\n");
    printf("Progress func: %s\n", status_to_str(status));
    printf("*******************************************\n");
  }

  close_controller(controller);
  return 0;
}

