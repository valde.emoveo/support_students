#include "table.h"
#include <stdlib.h>



Table create_table(size_t msize) {
  return (Table) {.array = create_array(sizeof(Entry), msize)};
}
void delete_table(Table *table) {
  step_array(&table->array, delete_entry);
  delete_array(&table->array);
}
bool empty_table(Table const *table) {
  return empty_array(&table->array);
}
bool full_table(Table const *table) {
  return full_array(&table->array);
}
int search_table(Table *table, Entry *entry, predicate condt) {
  return act_slct_array(&table->array, entry->key, condt, 
                        display_entry) ? EXIT_SUCCESS : KEY_NOT_FOUND;
}
int add_table(Table *table, Entry *entry) {
  if (predicate_array(&table->array, entry->key, is_entry)) {
    return KEY_DUPLICATE;
  }

  if (entry->parent.str != NULL && predicate_array(&table->array, entry, 
                                                   link_parent) == false) {
    return INVALID_PARENT;
  }
  return push_back(&table->array, entry);
}
int erase_table(Table *table, Entry *entry, predicate condt) {
  if (exist_table(table, entry, condt) == KEY_NOT_FOUND) {
    return KEY_NOT_FOUND;
  }
  
  for (size_t i = 0; i < table->array.top; ++i) {
    if (this_child(table->array.data[i], entry->key)) {
      unlink_parent(table->array.data + i);
//#include <stdio.h>
  //    Entry **ptr_entry = (Entry**)table->array.data + i;
 //     printf("Before %p:" (*ptr_entry)->parent.ptr);
      //Entry **ptr_entry = (Entry**)table->array.data + i;
   //   (*ptr_entry)->parent.ptr = NULL;

    //  printf("\tAfter%p:\n" (*ptr_entry)->parent.ptr);
    }
  }
//  predicate_array(&table->array, entry->key, unlink_parent);
  return erase_array(&table->array, entry->key, condt, delete_entry);
}
int exist_table(Table *table, Entry *entry, predicate condt) {
  return predicate_array(&table->array, entry->key, condt) ? 
            EXIT_SUCCESS: KEY_NOT_FOUND;
}
int display_table(Table *table) {
  step_array(&table->array, display_entry);
  return EXIT_SUCCESS;
}
