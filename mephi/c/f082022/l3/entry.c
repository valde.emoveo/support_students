#include "entry.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../common/input_func.h"


void input_entry(Entry *entry) {
  printf("Key: ");
  input_str(&entry->key);

  printf("Info: ");
  input_str(&entry->info);
  
  printf("Need enter parent key? ");
  if (yes_or_not() == true) {
    printf("Parent: ");
    input_str(&entry->parent.str);
  }
}
void input_srch_entry(Entry *entry) {
  printf("Key: ");
  input_str(&entry->key);
}
void delete_entry(void *data) {
  Entry *entry = (Entry*)data;
  free(entry->key);
  free(entry->info);
}
void display_entry(void *entry) {
  printf("**********\n");
  Entry const *data = (Entry const*) entry;
  printf("Key:  %s\nInfo: %s\n", data->key, data->info);
  printf("Parent: ");
  if (data->parent.ptr != NULL) {
    printf(" %s\n", data->parent.ptr->key);
  } else {
    printf(" (nil)\n");
  }
  printf("**********\n");
}
bool is_entry(void *entry, void *cmp_data) {
  return strcmp(((Entry*)entry)->key, (char*)cmp_data) == 0;
}
bool this_child(void *entry, void *cmp_data) {
  if (((Entry*)entry)->parent.ptr == NULL) {
    return false;
  }

  return strcmp(((Entry*)entry)->parent.ptr->key, cmp_data) == 0;
}
bool link_parent(void *entry, void *data) {
  if (is_entry(entry, ((Entry*)data)->parent.str) == false) {
    return false;
  }

  Entry *ptr_entry = (Entry*)data;
  free(ptr_entry->parent.str);
  ptr_entry->parent.ptr = (Entry*)entry;

  return true;
}
void unlink_parent(void *entry) {
  Entry **ptr_entry = (Entry**)entry;
  (*ptr_entry)->parent.ptr = NULL;
}
