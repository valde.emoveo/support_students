#ifndef MATRIX_H
#define MATRIX_H

#include <stddef.h>

typedef struct Line_t Line;
struct Line_t {
  size_t  size;
  int    *cell;
};

void        swap_line(Line *first, Line *second);
int         copy_line(Line *other, Line const *source);
void       input_line(Line *line);
void     display_line(Line const *line);

typedef struct Matrix_t Matrix;
struct Matrix_t {
  size_t  size;
  Line   *line;
};

Matrix* create_matrix(void);
void    delete_matrix(Matrix *matrix);
Matrix*   copy_matrix(Matrix const *source);
void   display_matrix(Matrix const *matrix);

#endif // MATRIX_H
