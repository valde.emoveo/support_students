#include "matrix.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../common/input_func.h"



void swap_line(Line *first, Line *second) {
  Line tmp = *first;
  *first = *second;
  *second = tmp;
}
int copy_line(Line *other, Line const *source) {
  if (other == NULL || source == NULL) {
    printf("Null ptr in %s\n", __func__);
    return EXIT_FAILURE;
  }
  
  other->size = source->size; 
  other->cell = calloc(source->size, sizeof(int));
  if (other->cell == NULL) {
    printf("%s: can't allocate memory for cell\n", __func__);
    return EXIT_FAILURE;
  }

  memcpy(other->cell, source->cell, source->size * sizeof(int));
  return EXIT_SUCCESS;
}
void input_line(Line *line) {
  if (line == NULL) {
    printf("Null ptr in %s\n", __func__);
    return;
  }

  line->size = 0;
  printf("Count colomn: ");
  input_correct_size(&line->size);
  
  line->cell = calloc(line->size, sizeof(int));
  for (size_t i = 0; i < line->size; ++i) {
    printf("[%zu] = ", i + 1);
    while (input_int(line->cell + i) == EXIT_FAILURE) {
      printf("Enter again\n[%zu] = ", i + 1);
    }
    //while (input_int(" = ", line->cell + i) == EXIT_FAILURE)
     // printf("Enter again\n[%zu]", i + 1);
  }
}
void display_line(Line const *line) {
  if (line == NULL) {
    printf("Null ptr in %s\n", __func__);
    return;
  }

  for (size_t i = 0; i < line->size; ++i) {
    printf("| %5d ", line->cell[i]);
  }
  printf("|\n");
}
Matrix* create_matrix(void) {
  Matrix *matrix = malloc(sizeof(Matrix));
  if (matrix == NULL) {
    printf("%s: can't allocate memory for matrix\n", __func__);
    return NULL;
  }

  matrix->size = 0;
  printf("Count lines: ");
  input_correct_size(&matrix->size);
  /*
  while (input_size("Count lines: ", &matrix->size) == EXIT_FAILURE)
    printf("Enter again\n");
    */

  matrix->line = calloc(matrix->size, sizeof(Line));
  if (matrix->line == NULL) {
    printf("%s: can't allocate memory for lines of matrix\n", __func__);
    free(matrix);
    return NULL;
  }
  
  for (size_t i = 0; i < matrix->size; ++i) {
    printf("\t\tLine #%zu\n", i + 1);
    input_line(matrix->line + i);
  }
  
  return matrix;
}
void delete_matrix(Matrix *matrix) {
  if (matrix == NULL) {
    printf("Null ptr in %s\n", __func__);
    return;
  }

//  while (matrix->size--)
  for (; matrix->size > 0; --matrix->size) {
    free(matrix->line[matrix->size - 1].cell);
  }

  free(matrix->line);
  free(matrix);
}
Matrix* copy_matrix(Matrix const *source) {
  if (source == NULL) {
    printf("Null ptr in %s\n", __func__);
    return NULL;
  }

  Matrix *other = malloc(sizeof(Matrix));
  if (other == NULL) {
    printf("%s: can't allocate memory for matrix\n", __func__);
    return NULL;
  }

  other->size = source->size;
  other->line = calloc(other->size, sizeof(Line));
  if (other->line == NULL) {
    printf("%s: can't allocate memory for lines of matrix\n", __func__);
    free(other);
    return NULL;
  }
  
  for (size_t i = 0; i < source->size; ++i) {
    if (copy_line(other->line + i, source->line + i) == EXIT_FAILURE) {
      other->size = i - 1;
      delete_matrix(other);
      other = NULL;
      break;
    }
  }

  return other;
}
void display_matrix(Matrix const *matrix) {
  if (matrix == NULL) {
    printf("Null ptr in %s\n", __func__);
    return;
  }

  for (size_t i = 0; i < matrix->size; ++i) {
    display_line(matrix->line + i);
  }
}
