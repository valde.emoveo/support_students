#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "matrix.h"


void display(char const *name, Matrix const *matrix) {
  printf("==================================================\n");
  printf("\t\t%s\n", name);
  display_matrix(matrix);
  printf("==================================================\n");
}
void bubble_sort(Matrix *matrix) {
  bool swapped = false;
  do {
    swapped = false;
    for (size_t i = 0; i < matrix->size - 1; ++i) {
      if (matrix->line[i].cell[0] > matrix->line[i + 1].cell[0]) {
        swap_line(matrix->line + i, matrix->line + i + 1);
        swapped = true;
      }
    }
  } while (swapped);
}
Matrix* sort_version(Matrix const *matrix) {
  Matrix *result = copy_matrix(matrix); 
  if (result == NULL)
    return NULL;

  bubble_sort(result);
  return result;
}


int main() {
  Matrix *input= create_matrix();
  if (input == NULL) {
    return 1;
  }

  display("Your matrix", input); 
  
  Matrix *output = sort_version(input);
  if (output != NULL) {
    display("Sort-version", output); 
    delete_matrix(output);
  }

  delete_matrix(input);

  return 0;
}
