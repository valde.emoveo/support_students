#ifndef INPUT_FUNC_H 
#define INPUT_FUNC_H 

#include <stddef.h>
#include <stdbool.h>

// (-2 147 483 647); (2 147 483 647)
#define MAX_LENGTH_STR_INT 10

int    str_to_int(char const *str, int *number);
char*   input_str(char **empty_str);
int     input_int(int *number);
int    input_size(size_t *size);
bool   yes_or_not();

int input_correct_size(size_t *size);

#endif // INPUT_FUNC_H 
