#ifndef ARRAY_H
#define ARRAY_H

#include <stddef.h>
#include <stdbool.h>

enum StatusArray {
  NULL_PTR = 2,
  NULL_DATA,
  ARRAY_OVERFLOW,
  MEMORY_OVERFLOW,
  EMPTY_ARRAY
};

typedef struct Array Array;
struct Array {
  size_t       top;
  size_t const sz_data;
  size_t const size; // max size
  void       **data;
};

Array create_array(size_t sz_data, size_t cnt_data);
void  delete_array(Array *array);

bool   empty_array(Array const *array);
bool    full_array(Array const *array);

int      push_back(Array *array, void const *data);
int       pop_back(Array *array, void *data);

// special func
typedef void (*action_data)(void*);
void      step_array(Array *array, action_data foo);

typedef bool (*predicate)(void*, void *);
bool predicate_array(Array *array, void *data, predicate act);

int   act_slct_array(Array *array, void *data, predicate slct,
                        action_data act);

int erase_array(Array *array, void *data, predicate condt,
                    action_data clear_content);

#endif /*ARRAY_H*/
