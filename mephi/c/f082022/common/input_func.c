#include "input_func.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>



int str_to_int(char const *str, int *number) {
  if (str == NULL || number == NULL) {
    return EXIT_FAILURE;
  }
  if (strlen(str) > MAX_LENGTH_STR_INT) {
    return EXIT_FAILURE;
  }
  
  long temp = 0;
  for (int i = 0; str[i] != '\0'; ++i) {
    if (str[i] < '0' || '9' < str[i]) {
      return EXIT_FAILURE;
    }

    temp = (temp * 10) + (str[i] - '0');
  }

  if (temp < INT_MIN || INT_MAX < temp) {
    return EXIT_FAILURE;
  }

  *number = (int) temp;
  return EXIT_SUCCESS;
}
char* input_str(char **str) {
  if (*str != NULL) {
    printf("Strint should be null in function\n");
    return NULL;
  }

  *str = (char*)malloc(16 * sizeof(char));
  if (*str == NULL) {
    printf("%s: can't allocate memory for string-line\n", __func__);
    return NULL;
  }

  size_t length = 0;
  while (1) {
    char sym = (char) getchar();
    if (sym == ' ' && length == 0) {
      continue;
    } else if (sym == '\n' || sym == '\0' || sym == EOF) {
      if (length == 0) {
        printf("Your input-data is empty\nEnter again: ");
        continue;
      }
      break;
    }

    (*str)[length++] = sym;
    if (length % 16 == 0) {
      *str = (char*) realloc(*str, length * 2 * sizeof(char));
    }
  }
  (*str)[length++] = '\0';

  *str = (char*) realloc(*str, length * sizeof(char));
  return *str;
}
int input_int(int *number) {
  char *str_int = NULL;
  if (input_str(&str_int) == NULL) {
    return EXIT_FAILURE;
  }

  bool is_positive = true;
  // Работа с знаками
  int pos_str = 0;
  for (; str_int[pos_str] != '\0'; ++pos_str) {
    if ('0' <= str_int[pos_str] && str_int[pos_str] <= '9') {
      break;
    }

    if (str_int[pos_str] == '-') {
      is_positive = !is_positive;
    } else if (str_int[pos_str] != '+') {
      break;
    }
  }
  
  int status = str_to_int(str_int + pos_str, number);
  free(str_int);
  
  if (status == EXIT_SUCCESS && is_positive == false) {
    *number *= (-1);
  }

  return status;
}
int input_size(size_t *size) {
  int temp_size = 0;
  if (input_int(&temp_size) == EXIT_FAILURE || temp_size <= 0) {
    return EXIT_FAILURE;
  }

  *size = (size_t) temp_size;
  return EXIT_SUCCESS;
}
bool yes_or_not() {
  printf("(Yes / no) ");
  bool result = false;
  char *choice = NULL;
  while (1) {
    input_str(&choice); 
    if (!strcmp(choice, "yes") || !strcmp(choice, "y") || 
        !strcmp(choice, "Y")) {

      result = true;
      break;

    } else if (!strcmp(choice, "no") || !strcmp(choice, "n") ||
        !strcmp(choice, "n")) {

      result = false;
      break;
    }

    printf("Invalid input: ");
    free(choice);
    choice = NULL;
  }

  free(choice);
  return result;
}
int input_correct_size(size_t *size) {
  if (size == NULL) {
    return EXIT_FAILURE;
  }

  while (input_size(size) == EXIT_FAILURE) {
    printf("Enter again: ");
  }

  return EXIT_SUCCESS;
}
