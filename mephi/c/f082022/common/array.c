#include "array.h"
#include <stdlib.h>
#include <string.h>



Array create_array(size_t sz_data, size_t cnt_data) {
  if (sz_data == 0) {
    exit(EXIT_FAILURE);
  }
  Array array = (Array) {
                 .top   = 0,
                 .sz_data = sz_data,
                 .size  = cnt_data,
                 .data = (void**) calloc(cnt_data, sizeof(void*))
                };

  if (array.data == NULL) {
    exit(EXIT_FAILURE);
  }
  return array;
}
void delete_array(Array *array) {
  if (array == NULL) {
    return;
  }

  step_array(array, free);
  free(array->data);
}
bool empty_array(Array const *array) {
  if (array == NULL) {
    return false;
  }
  return array->top == 0;
}
bool full_array(Array const *array) {
  if (array == NULL) {
    return false;
  }
  return array->top >= array->size;
}
int push_back(Array *array, void const *data) {
  if (array == NULL) {
    return NULL_PTR;
  }
  if (data == NULL) {
    return NULL_DATA;
  }
  if (full_array(array)) {
    return ARRAY_OVERFLOW;
  }
  
  array->data[array->top] = (void*)malloc(array->sz_data);
  if (array->data[array->top] == NULL) {
    return MEMORY_OVERFLOW;
  }
  memcpy(array->data[array->top], data, array->sz_data);

  array->top++;
  return EXIT_SUCCESS;
}
int pop_back(Array *array, void *data) {
  if (array == NULL) {
    return NULL_PTR;
  }
  if (data == NULL) {
    return NULL_DATA;
  }
  if (empty_array(array)) {
    return EMPTY_ARRAY;
  }
  
  --array->top;
  memcpy(data, array->data[array->top], array->sz_data);

  free(array->data[array->top]);
  array->data[array->top] = NULL;

  return EXIT_SUCCESS;
}
void step_array(Array *array, action_data foo) {
  if (array == NULL || foo == NULL) {
    return;
  }
  for (size_t i = 0; i < array->top; ++i) {
    foo(array->data[i]);
  }
}
bool predicate_array(Array *array, void *data, predicate act) {
  if (array == NULL || act == NULL) {
    return false;
  }

  for (size_t i = 0; i < array->top; ++i) {
    if (act(array->data[i], data)) {
      return true;
    }
  }
  return false;
}
int act_slct_array(Array *array, void *data, predicate slct,
                        action_data act) {
  if (array == NULL || slct == NULL || act == NULL) {
    return EXIT_FAILURE;
  }

  bool done = false;
  for (size_t i = 0; i < array->top; ++i) {
    if (slct(array->data[i], data)) {
      done = true;
      act(array->data[i]);
    }
  }
  return done;
}
int erase_array(Array *array, void *data, predicate condt,
                    action_data clear_content) {
  if (array == NULL || data == NULL || condt == NULL || clear_content == NULL) {
    return NULL_PTR;
  }

  size_t prev_top = array->top;
  for (size_t i = 0; i < array->top; ) {
    if (condt(array->data[i], data)) {
      clear_content(array->data[i]);
      free(array->data[i]);

      array->data[i] = array->data[array->top - 1];
      --array->top;
    } else {
      ++i;
    }
  }

  return prev_top != array->top ? EXIT_SUCCESS : EXIT_FAILURE;
}
